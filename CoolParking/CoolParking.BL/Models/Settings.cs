﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System.Collections.Generic;

namespace CoolParking.BL.Models

{
    public static class Settings {
        /*
     Начальный баланс Паркинга - 0;
    Вместимость Паркинга - 10;
    Период списания оплаты, N-секунд - 5;
    Период записи в лог, N-секунд - 60;
    Тарифы в зависимости от Тр. средства: Легковое - 2, Грузовое - 5, Автобус - 3.5, Мотоцикл - 1;
    коэффициент штрафа - 2.5.
        */
        public static readonly int parkingBalance = 0;
        public static readonly int parkingCapacity = 10;
        public static readonly int paymentPeriod = 5;
        public static readonly int logWritePeriod = 60;

        //PassengerCar, Truck, Bus, Motorcycle
        private static readonly Dictionary<VehicleType, double> priceForVehicle = new Dictionary<VehicleType, double> {
            { VehicleType.PassengerCar, 2 },
            { VehicleType.Truck, 5 },
            { VehicleType.Bus, 3.5 },
            { VehicleType.Motorcycle,1}
        };

        static readonly double fineCoefficient = 2.5;

    }
}