﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System.Text;
using System;

namespace CoolParking.BL.Models

{
    public class Vehicle 
    {
        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            Id = id;
            VehicleType = vehicleType;
            Balance = balance;
        }
        public Vehicle(VehicleType vehicleType, decimal balance)
        {
            Id = GenerateRandomRegistrationPlateNumber();
            VehicleType = vehicleType;
            Balance = 0;
        }

        public string Id { get;}
        public VehicleType VehicleType { get;}



        public decimal Balance { get; set; }
        
        public static string GenerateRandomRegistrationPlateNumber()
        {
            
            string letterPart = RandomString(4,25,65);

            string digitalPart= RandomString(4,10,48);
            return letterPart.Substring(0, 2) + "-" + digitalPart + "-" + letterPart.Substring(2, 2);
        }
        
        //Generate random string line
        private static string RandomString(int length, int amount, int start) {
            
            StringBuilder str_build = new StringBuilder();
            Random random = new Random();

            char letter;

            for (int i = 0; i < length; i++)
            {
                double flt = random.NextDouble();
                int shift = Convert.ToInt32(Math.Floor(amount * flt));
                letter = Convert.ToChar(shift + start);
                str_build.Append(letter);
            }
           

            return str_build.ToString();
                }
    }

}
  